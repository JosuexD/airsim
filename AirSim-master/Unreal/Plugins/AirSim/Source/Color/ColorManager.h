// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ColorManager.generated.h"

UCLASS()
class AIRSIM_API AColorManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AColorManager();

	UFUNCTION(BlueprintImplementableEvent)
		void Blueprint_FirstFunction(float r, float g, float b);

	UFUNCTION(BlueprintImplementableEvent)
		void Blueprint_SecondFunction();


	UFUNCTION()
		static void SecondFuction();

	UFUNCTION(BlueprintCallable)
		static void Init(UWorld* World);

	static const FSoftClassPath getColorActorPath()
	{
		return FSoftClassPath(TEXT("AActor'/Airsim/Blueprints/BP_ColorApi.BP_ColorApi_C"));
	}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
