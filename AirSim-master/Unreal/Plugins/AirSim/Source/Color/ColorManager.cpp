// Fill out your copyright notice in the Description page of Project Settings.


#include "ColorManager.h"
#include "Engine/World.h"


// Sets default values
AColorManager::AColorManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}
void AColorManager::SecondFuction()
{
	UE_LOG(LogTemp, Warning, TEXT("Second"));
	//FirstFunction();
}

void AColorManager::Init(UWorld* World)
{
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;

	UClass* ColorManagerClass = getColorActorPath().TryLoadClass<AActor>();


	AColorManager* self = World->SpawnActor<AColorManager>(ColorManagerClass, Location, Rotation, SpawnInfo);
	//selfThing = self;

}

// Called when the game starts or when spawned
void AColorManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AColorManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

